//1. Створіть об'єкт заробітної плати obj. Виведіть на екран зарплату Петі та Колі.
                
               //Цей об'єкт надано:
                    // var obj = {'Коля': '1000', 'Вася': '500', 'Петя': '200'};
         
var obj = {'Коля': '1000', 'Вася': '500', 'Петя': '200'};    

document.write("<p> Зарплата Петі:" + obj.Петя + "</p>")
document.write("<p> Зарплата Колі:" + obj.Коля + "</p>")




//2. Виведіть на сторінку назву валюти ціну купівлі та ціну продажу. https://api.privatbank.ua/p24api/exchange_rates?json&date=11.01.2021. Для виводу використовуєте html таблиці та стилі. переберіть обєкти викортстовуючи властивість values.

const tbody = document.getElementById("tbody");

let table=``;
data.exchangeRate.forEach(function(item, ind){
  table += `<tr><td>${ind+1}</td> <td>${item.currency}</td> <td>${item.saleRateNB.toFixed(2)}</td> <td>${item.purchaseRateNB.toFixed(2)}</td></tr>`
});

tbody.innerHTML = table;

//3. Створіть об'єкт cryptoWallet. У гаманці має зберігатись ім'я власника, кілька валют Bitcoin, Ethereum, Stellar 
// і в кожній валюті додатково є ім'я валюти, логотип, кілька монет та курс на сьогоднішній день.
// Також в об'єкті гаманець є метод при виклику якого він приймає ім'я валюти та виводить на сторінку інформацію.
// "Доброго дня, ... ! На вашому балансі (Назва валюти та логотип) залишилося N монет, якщо ви сьогодні продасте їх те, 
//отримаєте ...грн.

const cryptoWallet = {
userName: "Pupkin Vasylij",
bitcoin : {
  coinName:"Bitcoin",
  coinLogo:"https://s2.coinmarketcap.com/static/img/coins/64x64/1.png",
  coinExchange: 1115433.99,
  coinCount:594
},

ethereum : {
  coinName:"Ethereum",
  coinLogo:"https://i-invdn-com.investing.com/crypto-logos/80x80/v1/ethereum.png",
  coinExchange: 68835.77,
  coinCount: 1732
},

stellar : {
  coinName:"Stellar",
  coinLogo:"https://i-invdn-com.investing.com/crypto-logos/80x80/v1/stellar.png",
  coinExchange: 363.00,
  coinCount: 13974
},
show: function (coin){
document.write (`Доброго дня, ${this.userName}! На вашому балансі ${this[coin].coinName} <img src="${this[coin].coinLogo}"> 
залишилося ${this[coin].coinCount} монет, якщо ви сьогодні продасте їх то, 
отримаєте ${this[coin].coinCount * this[coin].coinExchange}грн.`)
}
}
cryptoWallet.show(prompt("Оберіть валюту", "bitcoin, ethereum, stellar"));

//4. Даний рядок типу 'var_text_hello'. Зробіть із нього текст 'VarTextHello'.

function ucfirst(str) {
    return str[0].toUpperCase() + str.substr(1);
   }
  
   var str = 'var_text_hello';
   var res = [];
  
   var newArr = str.split('_');
   res.push(newArr[0]);
  
   for (let i = 1; i < newArr.length; i++) {
     var newStr = ucfirst(newArr[i]);
     res.push(newStr);
   }
  
   var finish = res.join('');
  
   console.log(finish);

//5. Напишіть функцію isEmpty(obj), яка повертає true, якщо об'єкт не має властивостей, інакше false.

const obj1 = new Object();

function isEmpty (obj1) {
  for (let x in obj) {
    return console.log (true)
  }
    return console.log (false)
}
isEmpty(obj1)

//6. Зробіть функцію, яка рахує та виводить кількість своїх викликів.
let i=0;
function one (){
  i++
  console.log(i)
}

one()
one()
one()
one()