//1. Якщо змінна a дорівнює 10, то виведіть 'Вірно', інакше виведіть 'Неправильно'.

let а = 10;

if (а === 10) {
    console.log("Вірно")
} else {
    console.log("Неправильно")
}

//2. У змінній min лежить число від 0 до 59. Визначте, в яку чверть години потрапляє це число (У першу, другу, третю або четверту).
let min = parseFloat(prompt("Ведіть число 0-59"));

        if(min >=0 && min < 15){
            console.log("Перша")
        }else if(min >=0 && min < 30){
            console.log("Друга")
        }else if(min >=0 && min < 45){
            console.log("Третя")
        }else if (min >=0 && min < 60 ){
            console.log("Четверта")
        }

//3. Змінна num може приймати 4 значення: 1, 2, 3 або 4. Якщо вона має значення '1', то змінну result запишемо 'зима',
// якщо має значення '2' – 'весна' тощо. Розв'яжіть завдання через switch-case.

let num = parseFloat(prompt("Ведіть число 1, 2, 3 або 4"));

switch(num) {
    case "1" : console.log("Зима")
    case "2" : console.log("Весна")
    case "3" : console.log("Літо")
    case "4" : console.log("Осінь")
}

//4. Використовуючи цикли та умовні конструкції намалюйте ялинку
 //         *
 //        * *
 //       * * *
 //      * * * *
 //     * * * * *
 //         *
 //        * *
 //       * * *
 //      * * * *
 //     * * * * *

  for (let i = 0; i < 5; i++) {
   for (let k = 5; k > i - 1; k--) {
     document.write("&nbsp", "&nbsp")
   }
   for (let j = 0; j < i + 1; j++) {
     document.write("*", "&nbsp", "&nbsp")
   }
   document.write("<br>")
 }
 for (let i = 0; i < 5; i++) {
    for (let k = 5; k > i - 1; k--) {
      document.write("&nbsp", "&nbsp")
    }
    for (let j = 0; j < i + 1; j++) {
      document.write("*", "&nbsp", "&nbsp")
    }
    document.write("<br>")
  }

 

//5. Використовуючи умовні конструкції перевірте вік користувача, якщо користувачеві буде від 18 до 35 
//років переведіть його на сайт google.com через 2 секунди, якщо вік користувача буде від 35 до 60 років,
//переведіть його на сайт https://www.uz.gov.ua/, якщо користувачеві буде до 18 років покажіть йому першу серію лунтика з ютубу

let age = parseInt(prompt("Введіть вік 0-60"));

if(age > 0 && age < 18){
    document.write(`<meta http-equiv="refresh" content="2;URL=https://www.youtube.com/watch?v=icdSZKq9-sM">`);
}else if(age > 0 && age < 35){
    document.write(`<meta http-equiv="refresh" content="2;URL=https://google.com"></meta>`);
}else if (age > 0 && age < 60){
    document.write(`<meta http-equiv="refresh" content="2;URL=https://www.uz.gov.ua/"></meta>`);
}


//Виведіть непарні числа у проміжку між a і b
var a;
var b;

for ( a = a; a <= b; a ++ ) {
    if (a % 2 ==1) {
        console.log (a);
    }
}

// Пустий прямокутник

let vertLine = 10
let line = '**********'


document.write (line) + '</br>'

for (i = 0; i < vertLine-2; i++){
    document.write ('</br>' + '*' + '&nbsp&nbsp'.repeat(8) + '*' + '</br>' )
}

document.write (line)+'</br>'

// Заповнений прямокутник

let lin = '***************'

for (i = 0; i < 5; i++){
    document.write ('</br>' + lin )
}




// Прямокутний трикутник

var a = 9
var b = "&nbsp"

for ( i = 0; i < a; i++) {
    for (d = 0; d < i; d++)
    {document.write ("*")}

    document.write ("<br>")
}

// Рівносторонній трикутник

var a = 9
var b = "&nbsp"

for ( i = 0; i < a; i++) {
    for (c = a; c > i - 1; c-- ){
    document.write (b)
    }
    for (d = 0; d < i + 1; d++)
    {document.write ("*")}

    document.write ("<br>")
}


// Ромб

var a = 8
var b = "&nbsp"

for ( i = 0; i < a; i++) {
    for (c = a; c > i - 1; c-- ){
    document.write (b)
    }
    for (d = 0; d < i + 1; d++)
    {document.write ("*")}

    document.write ("<br>")
}

for ( i = 0; i < a-1; i++) {
        for (c = 0; c < i+3; c++ ){
        document.write (b)
        }
        for (d = 0; d < a - (i+1); d++)
        {document.write ("*")}
    
        document.write ("<br>")   

}


